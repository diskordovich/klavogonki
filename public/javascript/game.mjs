const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}
const playerList = document.getElementById("players")
const leaveButton = document.getElementById("leave-button")
const readyButton = document.getElementById("ready-button")
const modalButton = document.getElementById("modal-button")
const doneText = document.getElementById("done-text")
const currentText = document.getElementById("current-text")
const futureText = document.getElementById("future-text")
const hiddenText = document.getElementById("hidden-text")

let isReady = false;
const modalText = document.getElementById("modal-text")
const modal = document.getElementById("modal")

const socket = io("", { query: { username } });

const Leave = () => {
  sessionStorage.clear()
  window.location.replace("/login")
}

const StartGame = (toStart, id, text, gameTime) =>{
  let promise = new Promise((resolve, reject) =>{
    socket.on("alert",resolve)
    readyButton.style.visibility="hidden"
    leaveButton.style.visibility="hidden"
    hiddenText.style.visibility="visible"
    let fuText = "", curText = "", donText = "";
    curText=text[0]
    fuText=text.slice(1)
    currentText.innerHTML=curText
    futureText.innerHTML=fuText 
    document.addEventListener("keypress", (event)=>{
      if (event.key === curText[0]){
        donText+=curText
        curText=fuText[0]
        fuText = fuText.slice(1)


        doneText.innerHTML=donText
        currentText.innerHTML=curText
        futureText.innerHTML=fuText 
        console.log(currentText.innerHTML)
        socket.emit("plusScore", username)
      }
    })
    modalButton.addEventListener("click", resolve)
  }).then(()=>{
    doneText.innerHTML=""
    currentText.innerHTML=""
    futureText.innerHTML="" 
    hiddenText.style.width="0%"
    readyButton.style.visibility="visible"
    leaveButton.style.visibility="visible"
    hiddenText.style.visibility="hidden"
  })
}

const ModalAlert = (text, nextAction) => {
  let promise = new Promise((resolve, reject) =>{
    modal.style.zIndex=3
    modal.style.visibility = "visible"
    modalText.innerHTML = text
    modalButton.addEventListener("click", resolve)
  })
  promise.then(()=>{
    modal.style.zIndex=-1
    modal.style.visibility = "hidden"
  }).then(nextAction)
}

const ChangeReady = () =>{
  if(!isReady) {
    readyButton.innerHTML = "Unready"
    socket.emit("ready")
  }
  else { 
    readyButton.innerHTML = "Ready"
    socket.emit("unready")
  }
  isReady = !isReady;
  
}

const ReadyPlayer = ( id ) => {
  document.getElementById(id).firstChild.firstChild.className = "online-status ready"
}

const UnReadyPlayer = ( id ) => {
  document.getElementById(id).firstChild.firstChild.className = "online-status not-ready"
}

const CreatePlayer = (id, name) => {
  let Player = document.createElement("div")
  Player.id = id
  Player.className = "flex-vertical player-div"
  playerList.append(Player)

  let NameAndStatus = document.createElement("div")
  NameAndStatus.className = "flex"
  Player.append(NameAndStatus)

  let Status = document.createElement("div")
  Status.className = "online-status not-ready"
  NameAndStatus.append(Status)

  let Name = document.createElement("div")
  if (name === username) Name.innerHTML = name + " (you)"
  else Name.innerHTML = name
  Name.className = "Player-name"
  NameAndStatus.append(Name)

  let ProgressBar = document.createElement("div")
  ProgressBar.className = "status-bar"
  Player.append(ProgressBar)

  let Progress = document.createElement("div")
  Progress.className = "progress"
  ProgressBar.append(Progress)
}

const DeletePlayer = (id) => {
  document.getElementById(id).remove()
}

const DisconnectAccept = () =>{
  socket.emit("disconnectAccept")
}

socket.on("connect", ()=>{
  socket.emit("joinRequest", username)
})

socket.on("createPlayer", (id, name) => { 
  CreatePlayer(id, name)
})

socket.on("readyPlayer", (id) =>{
  ReadyPlayer(id)
})

socket.on("unReadyPlayer", (id) =>{
  UnReadyPlayer(id)
})

socket.on("deletePlayer", (id) => {
  DeletePlayer(id)
})

socket.on("disconnectAlert", (reason)=>{
  ModalAlert(`You were diconnected because ${reason}`, DisconnectAccept)
})

socket.on("startGame", (toStart, id, text, gameTime)=>{
  StartGame(toStart, id, text, gameTime)
})

socket.on("alert", (message) => {
  let textmessage = ""
  for (let str of message){
    textmessage+= `<p>${str.name}:${str.score} points </p>`
  }
  console.log(message)
  ModalAlert(textmessage, ()=>{})
})

socket.on("disconnect", Leave)
leaveButton.addEventListener("click", Leave)
readyButton.addEventListener("click", ChangeReady)




