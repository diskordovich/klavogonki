import { emit } from "nodemon";
import * as config from "./config.js";



export default io => {

  const checkIfReadyToStartAndStartIfReady = (socket) => {
    if (!isStart && PlayerList.size > 0) {
      isStart = true;

      for (let player of PlayerList.keys()) {
        if (!PlayerList.get(player).isReady) {
          isStart = false
          break
        }
      }



      if (isStart) {
        let id = Math.floor(Math.random() * 5)

        let Game = new Promise((resolve) => {
          socket.emit("startGame", config.SECONDS_TIMER_BEFORE_START_GAME, id, config.TEXT_MASS[id], config.SECONDS_FOR_GAME)
          socket.broadcast.emit("startGame", config.SECONDS_TIMER_BEFORE_START_GAME, id, config.TEXT_MASS[id], config.SECONDS_FOR_GAME)
          console.log("THE GAME HAS STARTED MUAHAHAHAHAHAHAHAHAHAHAHAHAH")

          const numOfLetters = config.TEXT_MASS[id].size

          setTimeout(resolve, (config.SECONDS_TIMER_BEFORE_START_GAME + config.SECONDS_FOR_GAME) * 1000)

          socket.on("plusScore", (username) => {
            let player = PlayerList.get(username)
            player.score++
            if (player.score >= numOfLetters) resolve()
            PlayerList.set(username, player)
          })
        }).then(() => {
          let WinnerMass = []
          for (let elem of PlayerList.keys()) {
            WinnerMass.push(
              {
                name: elem,
                score: PlayerList.get(elem).score
              }
            )
          }
          WinnerMass.sort((a, b) => {
            if (a.score > b.score) return 1
            if (a.score = b.score) return 0
            if (a.score < b.score) return -1
          })
          console.log(WinnerMass)
          socket.emit("alert", WinnerMass)
          socket.broadcast.emit("alert", WinnerMass)
          for (let player of PlayerList.keys()) {
            socket.emit("unReadyPlayer", PlayerList.get(player).id)
            socket.broadcast.emit("unReadyPlayer", PlayerList.get(player).id)
            let User = PlayerList.get(player)
            User.isReady = false
            PlayerList.set(player, User)
          }

          isStart = false
        })

      }
    }
  }


  let isStart = false;
  let PlayerList = new Map()



  io.on("connection", socket => {

    const username = socket.handshake.query.username;
    socket.validEnter = false
    console.log(PlayerList)

    if (PlayerList.has(username)) {
      socket.emit("disconnectAlert", "the username is already taken")
      socket.on("disconnectAccept", socket.disconnect)
    }
    else if (!socket.id) {
      socket.emit("disconnectAlert", "socket id is not present")
      socket.on("disconnectAccept", socket.disconnect)
    }
    else if (PlayerList.size >= config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      socket.emit("disconnectAlert", "too much people in the room")
      socket.on("disconnectAccept", socket.disconnect)
    }
    else {
      PlayerList.set(username, {
        id: socket.id,
        isReady: false,
        score: 0
      })
      socket.validEnter = true
      console.log(socket.validEnter)

      for (let player of PlayerList.keys()) {
        socket.emit("createPlayer", PlayerList.get(player).id, player)
        if (PlayerList.get(player).isReady) socket.emit("readyPlayer", PlayerList.get(player).id)
      }

      socket.broadcast.emit("createPlayer", PlayerList.get(username).id, username)
    }





    socket.on("disconnect", () => {
      console.log("disconnect")
      if (socket.validEnter) {
        socket.broadcast.emit("deletePlayer", PlayerList.get(username).id)
        PlayerList.delete(username)
        checkIfReadyToStartAndStartIfReady(socket)
      }
    })

    socket.on("ready", () => {
      socket.emit("readyPlayer", PlayerList.get(username).id)
      socket.broadcast.emit("readyPlayer", PlayerList.get(username).id)

      let player = PlayerList.get(username)
      player.isReady = true
      PlayerList.set(username, player)

      checkIfReadyToStartAndStartIfReady(socket)
    })

    socket.on("unready", () => {
      socket.emit("unReadyPlayer", PlayerList.get(username).id)
      socket.broadcast.emit("unReadyPlayer", PlayerList.get(username).id)

      let player = PlayerList.get(username)
      player.isReady = false
      PlayerList.set(username, player)
    })

  });

};
