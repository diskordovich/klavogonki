export const MAXIMUM_USERS_FOR_ONE_ROOM = 5;
export const SECONDS_TIMER_BEFORE_START_GAME = 2;
export const SECONDS_FOR_GAME = 10;
export const TEXT_MASS = [
    "Peter Piper picked a peck of pickled peppers",
    "Betty Botter bought some butter But she said the butter’s bitter",
    "How much wood would a woodchuck chuck if a woodchuck could chuck wood?",
    "How can a clam cram in a clean cream can?",
    "Fuzzy Wuzzy was a bear. Fuzzy Wuzzy had no hair. Fuzzy Wuzzy wasn’t fuzzy, was he?"
]
